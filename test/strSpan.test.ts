import { StrSpan } from "../src/strSpan"

describe("StrSpan", () => {
  it("skip works with zero length", () => {
    const orig = new StrSpan({input: "hello world"})
    const next = orig.skip(0)

    expect(orig).toEqual(next)
  })

  it("skp works with length 1", () => {
    const orig = new StrSpan({input: "hello world"})
    const next = orig.skip(1)

    expect(next.text).toEqual(orig.text.substring(1))
  })

  it("skp works with length 5", () => {
    const orig = new StrSpan({input: "hello world"})
    const next = orig.skip(5)

    expect(next.text).toEqual(orig.text.substring(5))
  })
})