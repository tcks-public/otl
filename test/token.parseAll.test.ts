import { StrSpan } from "../src/strSpan"
import { Token } from "../src/token"
import { TokenKind, TokenKindPriority } from "../src/tokenKind"

describe("Token.parseAll", () => {
  it("can parse few tokens", () => {
    const code = new StrSpan({input: "hello world"})
    const tokens = Token.parseAll(code)
    const actual = tokens.map(x => [x.code.toString(), x.kind])

    const expected = [
      ["hello", TokenKind.identifier],
      [" ", TokenKind.whitespace],
      ["world", TokenKind.identifier]
    ]

    expect(expected).toEqual(actual)
  })

  it("can parse few tokens even with uknown ones", () => {
    const code = new StrSpan({input: "hello~ˇ^˘°`˙´world"})
    const tokens = Token.parseAll(code)
    const actual = tokens.map(x => [x.code.toString(), x.kind])

    const expected = [
      ["hello", TokenKind.identifier],
      ["~ˇ^˘°`˙´", TokenKind.unknown],
      ["world", TokenKind.identifier]
    ]

    expect(expected).toEqual(actual)
  })

  it("can parse few tokens even with uknown ones (without merging)", () => {
    const code = new StrSpan({input: "hello~ˇ^˘°`˙´world"})
    const tokens = Token.parseAll(code, {without: {merging: true}})
    const actual = tokens.map(x => [x.code.toString(), x.kind])

    const expected = [
      ["hello", TokenKind.identifier],
      ["~", TokenKind.unknown],
      ["ˇ", TokenKind.unknown],
      ["^", TokenKind.unknown],
      ["˘", TokenKind.unknown],
      ["°", TokenKind.unknown],
      ["`", TokenKind.unknown],
      ["˙", TokenKind.unknown],
      ["´", TokenKind.unknown],
      ["world", TokenKind.identifier]
    ]

    expect(expected).toEqual(actual)
  })
})