import { TokenKind } from "../src/tokenKind"

describe("TokenKind", () => {
  it("identifier regex is correct", () => {
    testTokenKindRegex(TokenKind.identifier, {
      correctCases: [
        "a", "_a", "a_",
        "ab", "_ab", "a_b", "ab_",
        "_a_b", "_ab_", "a_b_",
        "_a_b_",
  
        "_1",
        "a1", "ab1", "a1b", "_a_1_b_1"
      ],
      incorrecrtCases: [
        "", " a", "\na", "1a", ".a", "-a", "1"
      ]
    })
  })
})

function testTokenKindRegex(tokenKind:TokenKind, options:{
  correctCases: string[],
  incorrecrtCases: string[]
}) {
  testCorrectCases(tokenKind, options.correctCases)
  testIncorrectCases(tokenKind, options.incorrecrtCases)
}

function testCorrectCases(tokenKind:TokenKind, correctCases:string[]) {
  const rx = tokenKind.regex.compile()

  const errors = []
  let counter = 0
  for(const correctCase of correctCases) {
    counter += 1

    const rxResult = rx.exec(correctCase)
    const match0 = rxResult[0]
    if (match0 == null) {
      errors.push(`"${correctCase} => null"`)
      continue
    }

    if (rxResult.index != 0) {
      errors.push("Match index != 0.")
      continue
    }

    continue
  }

  expect(errors).toEqual([])
  expect(counter).toEqual(correctCases.length)
}

function testIncorrectCases(tokenKind:TokenKind, incorrectCases:string[]) {
  const rx = tokenKind.regex.compile()

  const errors = []
  let counter = 0
  for(const incorrectCase of incorrectCases) {
    counter += 1

    const rxResult = rx.exec(incorrectCase)
    if (rxResult.index == 0) {
      errors.push(incorrectCase)
      continue
    }

    const match0 = rxResult[0]
    if (match0 != null) {
      errors.push(incorrectCase)
      continue
    }

    continue
  }

  expect(errors).toEqual(incorrectCases)
  expect(counter).toEqual(incorrectCases.length)
}