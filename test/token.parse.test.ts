import { StrSpan } from "../src/strSpan"
import { Token } from "../src/token"
import { TokenKind, TokenKindPriority } from "../src/tokenKind"

describe("Token.parse", () => {
  it("can parse whitespace", () => {
    const testWhitespace = makeTokenParseTest(TokenKind.whitespace)

    testWhitespace(" aloha", " ")
    testWhitespace("  aloha ", "  ")
    testWhitespace("\t \n aloha ", "\t \n ")
  })

  it("can parse identifier", () => {
    const testIdentifier = makeTokenParseTest(TokenKind.identifier)

    testIdentifier("x", "x")
    testIdentifier("x ", "x")
    testIdentifier("x-", "x")

    testIdentifier("aloha", "aloha")
    testIdentifier("aloha ", "aloha")
    testIdentifier("aloha-", "aloha")
    testIdentifier("aloha+", "aloha")

    testIdentifier("_", "_")
    testIdentifier("_ ", "_")
    testIdentifier("_-", "_")

    testIdentifier("_1", "_1")
    testIdentifier("_1 ", "_1")
    testIdentifier("_1+", "_1")
  })

  it("can parse integer literal", () => {
    const testIntegerLiteral = makeTokenParseTest(TokenKind.integerLiteral)

    testIntegerLiteral("0", "0")
    testIntegerLiteral("0 ", "0")
    testIntegerLiteral("0-", "0")

    testIntegerLiteral("01", "01")
    testIntegerLiteral("01 ", "01")
    testIntegerLiteral("01+", "01")

    testIntegerLiteral("01_", "01_")
    testIntegerLiteral("01_ ", "01_")
    testIntegerLiteral("01_+", "01_")

    testIntegerLiteral("01_123", "01_123")
    testIntegerLiteral("01_123 ", "01_123")
    testIntegerLiteral("01_123+*", "01_123")
  })

  it("can parse decimal literal", () => {
    const testDecimalLiteral = makeTokenParseTest(TokenKind.decimalLiteral)

    testDecimalLiteral("1.0", "1.0")
    testDecimalLiteral("1.0 ", "1.0")
    testDecimalLiteral("1.0-", "1.0")
    testDecimalLiteral("1.0.", "1.0")
    testDecimalLiteral("1.0)", "1.0")
  })

  it("can parse string literal", () => {
    const testStringLiteral = makeTokenParseTest(TokenKind.stringLiteral)

    testStringLiteral('""', '""')
    testStringLiteral('" "', '" "')
    testStringLiteral('"0"', '"0"')
    testStringLiteral('"012_3"', '"012_3"')
  })

  it("can parse string literal (with double-quotes inside)", () => {
    const r = String.raw
    const testStringLiteral = makeTokenParseTest(TokenKind.stringLiteral)

    testStringLiteral(r`"\""`, r`"\""`)
    testStringLiteral(r`" \""`, r`" \""`)
    testStringLiteral(r`"\" "`, r`"\" "`)
    testStringLiteral(r`" \" "`, r`" \" "`)
    testStringLiteral(r`" \" asdf \" "`, r`" \" asdf \" "`)
  })

  it ("can parse special symbol", () => {
    const testSpecialSymbol = makeTokenParseTest(TokenKind.specialSymbol)

    // Special symbol ::= "(" | ")" | "[" | "]" | "{" | "}"
    // | "&&" | "||" | "+" | "-" | "*" | "/" | "%"
    // | "." | "," | "<" | "<=" | ">" | ">=" | "==" | "!="

    {
      testSpecialSymbol("(", "(")
      testSpecialSymbol("( ", "(")
      testSpecialSymbol("(-", "(")
      testSpecialSymbol("((", "(")

      testSpecialSymbol(")", ")")
      testSpecialSymbol(") ", ")")
      testSpecialSymbol(")-", ")")
      testSpecialSymbol("))", ")")
    }

    {
      testSpecialSymbol("[", "[")
      testSpecialSymbol("[ ", "[")
      testSpecialSymbol("[-", "[")
      testSpecialSymbol("[[", "[")

      testSpecialSymbol("]", "]")
      testSpecialSymbol("] ", "]")
      testSpecialSymbol("]-", "]")
      testSpecialSymbol("]]", "]")
    }

    {
      testSpecialSymbol("{", "{")
      testSpecialSymbol("{ ", "{")
      testSpecialSymbol("{-", "{")
      testSpecialSymbol("{{", "{")

      testSpecialSymbol("}", "}")
      testSpecialSymbol("} ", "}")
      testSpecialSymbol("}-", "}")
      testSpecialSymbol("}}", "}")
    }

    {
      testSpecialSymbol("/", "/")
      testSpecialSymbol("/ ", "/")
      testSpecialSymbol("/-", "/")
      testSpecialSymbol("/(", "/")
    }

    const otherSymbols = ["+", "-", "%", "*", "=", "&", "|", ".", ",", "<", ">", "!"]
    for(const otherSymbol of otherSymbols) {
      testSpecialSymbol(otherSymbol, otherSymbol)
      testSpecialSymbol(`${otherSymbol} `, otherSymbol)
      testSpecialSymbol(`${otherSymbol}-`, otherSymbol)
      testSpecialSymbol(`${otherSymbol}${otherSymbol}`, otherSymbol)
    }
  })

  it("can parse single line comment", () => {
    const testComment = makeTokenParseTest(TokenKind.singleLineComment)

    testComment("//kuku", "//kuku")
    testComment("//kuku\n", "//kuku")
    testComment("//kuku\nlala", "//kuku")
  })

  it("can parse multi line comment", () => {
    const testComment = makeTokenParseTest(TokenKind.multiLineComment)

    testComment("/**/", "/**/")
    testComment("/**/ ", "/**/")
    testComment("/**//", "/**/")
    testComment("/**/*", "/**/")
    testComment("/**//*", "/**/")

    testComment("/* */", "/* */")
    testComment("/*//*/", "/*//*/")
    testComment("/*\n*/", "/*\n*/")
    testComment("/*//\n//*/", "/*//\n//*/")
    testComment("/*/*/*/**/", "/*/*/*/**/")
  })

  it("can parse new line", () => {
    const testNewLine = makeTokenParseTest(TokenKind.endOfLine)

    testNewLine("\n", "\n")
    testNewLine("\n ", "\n")
    testNewLine("\n\t", "\n")
    testNewLine("\na", "\n")

    testNewLine("\r", "\r")
    testNewLine("\r ", "\r")
    testNewLine("\r\t", "\r")
    testNewLine("\ra", "\r")

    testNewLine("\n\r", "\n\r")
    testNewLine("\n\r ", "\n\r")
    testNewLine("\n\r\t", "\n\r")
    testNewLine("\n\ra", "\n\r")

    testNewLine("\r\n", "\r\n")
    testNewLine("\r\n ", "\r\n")
    testNewLine("\r\n\t", "\r\n")
    testNewLine("\r\na", "\r\n")
  })
})

function testTokenParse(input: string | StrSpan, expectedKind: TokenKind, expectedText: string) {
  const code = new StrSpan({input: input})
  const token = Token.parse(code)

  expect({text: token.code.toString(), kind: token.kind})
    .toEqual({text: expectedText.toString(), kind: expectedKind})
}

function makeTokenParseTest(expectedKind: TokenKind) {
  return (input: string | StrSpan, expectedText: string) => testTokenParse(input, expectedKind, expectedText)
}