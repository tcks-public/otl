export const enum TokenKindPriority {
  Top = 0,
  First = 1,
  Second = 2,

  Basic = 1000,
  PreBasic = 999,

  Last = 9999
}

export type TokenKind = {
  options: {
    priority: TokenKindPriority,
    canMerge: boolean
  }
  name: string
  regex: RegExp
}

export module TokenKind {
  // >>> ignored trivias >>>
  export const whitespace = makeTokenKind("whitespace", /\s+/)
  // <<< ignored trivias <<<

  // Identifier ::= ( Letter | "_" ) { Letter | "_" | Digit }
  export const identifier = makeTokenKind("identifier", /[_a-zA-Z][_a-zA-Z0-9]*/)

  // Integer literal ::= Digit { Digit }
  export const integerLiteral = makeTokenKind("integerLiteral", /\d[_\d]*/)

  // **custom**
  // Decimal literal ::= Digit { Digit } "." Digit { Digit }
  export const decimalLiteral = makeTokenKind("decimalLiteral", /\d[_\d]*\.[_\d]+/)

  // String literal ::= """ { Character or escape } """
  export const stringLiteral = makeTokenKind("stringLiteral", /"((\s|\w)*(\\")*)*"/)

  // Character or escape ::= "\r" | "\n" | "\"" | "\\" | Non-escape character

  // Non-escape character ::= _any UNICODE codepoint except backslash, quote or EOL marker_

  // Character ::= _any UNICODE codepoint except EOL marker_

  // Letter ::= "a" | … | "z" | "A" | … "Z"

  // Digit ::= "0" | "1" | … | "9"

  // Keyword ::= public | private | class | byte | int | string | bool | if | else | while | true | false | new

  // Special symbol ::= "(" | ")" | "[" | "]" | "{" | "}" | "&&" | "||" | "+" | "-" | "*" | "/" | "%" | "." | "," | "<" | "<=" | ">" | ">=" | "==" | "!="
  export const specialSymbol = makeTokenKind("specialSymbol", /[\*\(\)\[\]\{\}\&\|\+\-\/\%\.\,\<\=\>\!]/)

  // Single line comment ::= "//" { Character } End of line
  export const singleLineComment = makeTokenKind("singleLineComment", /\/\/.*($|\n{0})/, {
    priority: TokenKindPriority.PreBasic
  })

  // Multi line comment ::= "/*" { Character | End of line } "*/"
  export const multiLineComment = makeTokenKind("multiLineComment", /\/\*[\s\S]*\*\//, {
    priority: TokenKindPriority.PreBasic
  })

  // End of line ::= _any EOL marker_
  export const endOfLine = makeTokenKind("endOfLine", /(\n|\r)+/, {
    priority: TokenKindPriority.PreBasic
  })

  export const knownTokenKinds = Object.freeze([
    whitespace,
    identifier,
    integerLiteral,
    decimalLiteral,
    stringLiteral,
    specialSymbol,
    singleLineComment,
    multiLineComment,
    endOfLine
  ])

  // this is special fallback token kind for all others tokens
  export const unknown = makeTokenKind("unknown", ".", {
    priority: TokenKindPriority.Last,
    canMerge: true
  })
  
  export const allTokenKinds = Object.freeze(knownTokenKinds.concat([unknown]))
}

function makeTokenKind(name:string, regex:RegExp | string, options?:{
  priority?:TokenKindPriority
  canMerge?: boolean
}):TokenKind {
  return {
    name: name,
    regex: typeof regex == "string" ? new RegExp(regex) : regex,
    options: {
      priority: options?.priority ?? TokenKindPriority.Basic,
      canMerge: options?.canMerge ?? false
    }
  }
}