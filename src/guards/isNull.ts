export function isNull<T>(input:T):input is null | undefined {
  return input == null
}

export function isNullExactly<T>(input:T):input is null {
  return input === null
}