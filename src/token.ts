import { StrSpan } from "./strSpan";
import { TokenKind } from "./tokenKind";

export type Token = {
  code: StrSpan
  kind: TokenKind
}

export module Token {
  type ParseAllOptions = {
    without?:{
      merging?: boolean
    }
  }

  export function parseAll(code:StrSpan, options?:ParseAllOptions):Token[] {
    const result:Token[] = []

    let currentCode = code
    while(currentCode.length > 0) {
      const lastCodeLength = currentCode.length

      const token = parse(currentCode)
      if (token == null) throw new Error(`Token was not parsed.\nCode:${currentCode}`)

      result.push(token)
      currentCode = currentCode.skip(token.code.length)

      // safety check
      if (lastCodeLength <= currentCode.length) {
        throw new Error(`Last code length(${lastCodeLength}) <= current code length(${currentCode.length}).`)
      }
    }

    if (options?.without?.merging) {
      return result
    }

    const mergedResult = this.mergeTokens(result)
    return mergedResult
  }

  export function mergeTokens(tokens:Token[]) {
    if (tokens == null || tokens.length < 1) return tokens

    const result:Token[] = []

    let lastToken = tokens[0]
    for(let i = 1; i < tokens.length; i++) {
      const currToken = tokens[i]
      const currKind = currToken.kind

      if (currKind.name == lastToken.kind.name && currKind.options.canMerge) {
        lastToken = this.mergeTwoTokens(lastToken, currToken)
      }
      else {
        result.push(lastToken)
        lastToken = currToken
      }
    }

    result.push(lastToken)

    return result
  }

  export function mergeTwoTokens(first:Token, second:Token):Token {
    if (first.kind.name != second.kind.name) {
      throw new Error(`Merge of two tokens failed.\nFirst: ${first.kind}\nSecond: ${second.kind}`)
    }

    const firstCode = first.code
    const newCode = new StrSpan({
      input: firstCode.parent,
      start: firstCode.start,
      length: firstCode.length + second.code.length
    })

    const newText = newCode.text
    const expectedText = firstCode.text + second.code.text
    if (newText != expectedText) {
      throw new Error(`Mergin of two tokens failed.\nExpected text:${expectedText}\nNew text:${newText}`)
    }

    return {
      code: newCode,
      kind: first.kind
    }
  }

  export function parse(code:StrSpan):Token {
    const str = code.text
    const matchPairs = TokenKind.allTokenKinds
    .map(tk => ({
      tk: tk,
      match: tk.regex.exec(str)
    }))
    .filter(x => x.match != null)
    .filter(x => x.match.index == 0)
    .sort((a, b) => 
      (a.tk.options.priority - b.tk.options.priority)
      || ((b?.match?.[0]?.length ?? 0) - (a?.match?.[0]?.length ?? 0))
    )

    const bestMatchPair = matchPairs[0]
    if (bestMatchPair == null) return null

    const bestRxMatch = bestMatchPair.match[0]

    const span = new StrSpan({
      input: code,
      start: 0,
      length: bestRxMatch.length
    })

    return {
      code: span,
      kind: bestMatchPair.tk
    }
  }
}