export class StrSpan {
  readonly parent?:ParentOfStrSpan
  readonly start:number
  readonly length:number
  readonly text:string

  readonly isRoot:boolean
  readonly isEmpty:boolean

  #totalStart:number
  get totalStart() {
    return this.#totalStart ?? (
      this.#totalStart = this.start + getParentTotalStart(this.parent)
    )
  }

  substr(from: number, length?: number): string {
    return this.text.substr(from, length)
  }

  toString() {
    return this.text
  }

  skip(skipLength:number) {
    if (skipLength <= 0) return this
    
    return new StrSpan({
      input: this,
      start: skipLength
    })
  }

  constructor(options:CtorOptions) {
    const input = options.input ?? ""

    this.parent = input
    this.start = options.start ?? 0
    this.length = options.length ?? input.length - this.start
    this.text = input.substr(this.start, this.length)
    this.isEmpty = this.length < 1
    this.isRoot = this.start == 0 &&
                  this.length == input.length &&
                  (typeof input == "string" || input.isRoot)
  }
}

type ParentOfStrSpan = string | StrSpan

export type CtorOptions = {
  input: ParentOfStrSpan
  start?: number
  length?: number
}

function getParentTotalStart(parent:ParentOfStrSpan) {
  return typeof parent == "string" ? 0 : parent?.totalStart ?? 0
}